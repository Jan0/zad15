# Moje Repozytorium


## teksty
 **elo**, _elo_ i ~~elo~~.

## Cytat
> "Never stray from the Way.”
― Miyamoto Musashi

## Listy

### Listy Numeryczne
1. Pierwszy element
   1. Podpunkt 1
   2. Podpunkt 2
2. Drugi element
3. Trzeci element

### Listy Nienumeryczne
- Element A
  - Podpunkt A1
  - Podpunkt A2 `print("elo")`.
- Element B
- Element C


## Obraz

![narty](narty.jpg)

## Blok Kodu

```python
def hello_world():
    print("Hello, World!")

hello_world()




